//
//  productsmodel.swift
//  CodingAssignment
//
//  Created by Anvesh on 02/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import Foundation

class productsmodel:NSObject {
    let name : String
    let  price : String
    let imageurl : String
    let Product_id : String
    
    init(dict : NSDictionary) {
        name = dict.value(forKey: "name") as! String
        price = dict.value(forKey: "price") as! String
        imageurl = dict.value(forKey: "imageurl") as! String
        Product_id = dict.value(forKey: "Product_id") as! String
        
    }
    
}
