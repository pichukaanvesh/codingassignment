//
//  BaseVC.swift
//  CodingAssignment
//
//  Created by Anvesh on 01/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import UIKit
import CoreData

class BaseVC: UIViewController {
 
    var networkReachability = Reachability()
    var cartproducts = [productsmodel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    func clearcartData()  {
        
        let managedContext = globalclass.shared.managedContext
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Products")
        
        
        
        do {
            let products = try managedContext.fetch(fetchRequest)
            if products.count > 0 {
                for product in products{
                    managedContext.delete(product)
                    try managedContext.save()
                }
            }
            
            
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    

        
        func showToast(message : String, font: UIFont) {
            
            let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            toastLabel.textColor = UIColor.white
            toastLabel.font = font
            toastLabel.textAlignment = .center;
            toastLabel.text = message
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds  =  true
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 2.0, delay: 0.1, options: .allowAnimatedContent, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                
                toastLabel.removeFromSuperview()
            })
        } 

}
