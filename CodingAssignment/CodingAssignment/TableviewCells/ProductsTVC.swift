//
//  ProductsTVC.swift
//  CodingAssignment
//
//  Created by Anvesh on 02/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import UIKit

class ProductsTVC: UITableViewCell {

    @IBOutlet weak var product_Name: UILabel!
    @IBOutlet weak var product_Image: UIImageView!
    @IBOutlet weak var product_Price: UILabel!
    @IBOutlet weak var cart_Btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
