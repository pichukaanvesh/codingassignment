//
//  CheckOutTVC.swift
//  CodingAssignment
//
//  Created by Anvesh on 02/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import UIKit

class CheckOutTVC: UITableViewCell {
    @IBOutlet weak var product_image: UIImageView!
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var price_Label: UILabel!
    @IBOutlet weak var remove_Cart: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
