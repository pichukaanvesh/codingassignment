//
//  ApiManager.swift
//  CodingAssignment
//
//  Created by Anvesh on 02/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import Foundation
class APImanager: NSObject {
    
  class func getproduts( sucess: @escaping ( _ products:[productsmodel])->(),failure: @escaping ( _ error : Error)->())
    {
        var result = [productsmodel]()
        if let path = Bundle.main.path(forResource: "products", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSDictionary
                for product in jsonResult.value(forKey: "products") as! NSArray{
                    
                    let newproduct = productsmodel.init(dict: product as! NSDictionary)
                    result.append(newproduct)
                    
                    
                }
                sucess(result)
            } catch {
                failure(error)
                
            }
        }
        
    }
    

}
