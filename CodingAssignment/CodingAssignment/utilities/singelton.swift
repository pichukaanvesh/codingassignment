//
//  singelton.swift
//  CodingAssignment
//
//  Created by Anvesh on 02/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import Foundation
import CoreData
import  UIKit

class globalclass {
    static let shared = globalclass()
    var managedContext: NSManagedObjectContext
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    private init(){
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    
}
