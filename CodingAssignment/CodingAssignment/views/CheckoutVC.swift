//
//  CheckoutVC.swift
//  CodingAssignment
//
//  Created by Anvesh on 02/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import UIKit
import CoreData

class CheckoutVC: BaseVC {
    @IBOutlet weak var checkOutTV: UITableView!
    @IBOutlet weak var price_Label: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    var Cartproducts = [productsmodel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cart"
self.navigationController?.navigationBar.isHidden = false
        self.fetchcartData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func checkoutAction(_ sender: Any) {
        
    }
    func fetchcartData()  {
       Cartproducts.removeAll()
        let managedContext = globalclass.shared.managedContext
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Products")
        
        
    
        do {
            let products = try managedContext.fetch(fetchRequest)
            for product in products{
                var dict = Dictionary<String,String>()
                dict["Product_id"] = (product.value(forKey: "productID") as! String)
                dict["imageurl"] = (product.value(forKey: "productImage") as! String)
                dict["price"] = (product.value(forKey: "productPrice") as! String)
                dict["name"] = (product.value(forKey: "productName") as! String)
                let cartproduct = productsmodel.init(dict: dict as NSDictionary)
                Cartproducts.append(cartproduct)
               
            }
            print(Cartproducts)
            self.calculatetotalprice()
            checkOutTV.reloadData()

            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    func calculatetotalprice()  {
        var totalPrice = 0
        for product in Cartproducts{
            var productprice = product.price
          productprice = productprice.replacingOccurrences(of: ",", with: "")
            let price = Int(productprice)
            totalPrice = totalPrice + price!
        
        }
        price_Label.text = "Rs : " + String(totalPrice)
        
    }
    
    func removeitem(productid:String)  {
        let managedContext = globalclass.shared.managedContext
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Products")
        fetchRequest.predicate =  NSPredicate(format: "productID == %@", productid)
        
        //3
        do {
            let product1 = try managedContext.fetch(fetchRequest)
            if product1.count > 0 {
                managedContext.delete(product1[0])
                try managedContext.save()
                self.fetchcartData()
                self.showToast(message: "Removed from Cart", font: UIFont.systemFont(ofSize: 14, weight: .black))
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
}
extension CheckoutVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return Cartproducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let product = Cartproducts[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckOutTVC", for: indexPath) as! CheckOutTVC
        cell.product_name.text = product.name
        cell.price_Label.text = product.price
        cell.product_image?.image = UIImage(named: product.imageurl)
        cell.remove_Cart.tag = indexPath.row
        cell.remove_Cart.addTarget(self, action: #selector(handleremovecart(sender:)), for:.touchUpInside)
        return cell
    }
    @objc func handleremovecart(sender:UIButton)
    {
        
        self.removeitem(productid: Cartproducts[sender.tag].Product_id)
    }
    
    
    
}








