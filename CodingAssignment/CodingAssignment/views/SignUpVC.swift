//
//  SignUpVC.swift
//  CodingAssignment
//
//  Created by Anvesh on 01/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import UIKit
import CoreData

class SignUpVC: BaseVC {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUPAction(_ sender: Any) {
        if emailTF.text != "" && passwordTF.text != "" && confirmPasswordTF.text != "" {
            if passwordTF.text == confirmPasswordTF.text{
                self.clearcartData()
                UserDefaults.standard.setValue(emailTF.text, forKey: "username")
                UserDefaults.standard.setValue(passwordTF.text, forKey: "password")
         UserDefaults.standard.set(true, forKey: "LoginStatus")
        let Dashboardvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        self.navigationController?.pushViewController(Dashboardvc, animated: true)
        }
            else {
                self.showAlert(title: "error", message: "password and confirm password didnt match", actionTitles: ["ok"], actions: [{action1 in
                    print("ok")
                },nil])
            }
        }
        else{
            self.showAlert(title: "All fields are required", message: "please fill all the fields", actionTitles: ["ok"], actions: [{ action1 in
                print("ok")
            },nil])
        }
        
    }
    
    @IBAction func signinAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        
    }
    

}
