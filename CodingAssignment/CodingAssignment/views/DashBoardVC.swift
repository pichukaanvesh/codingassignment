//
//  DashBoardVC.swift
//  CodingAssignment
//
//  Created by Anvesh on 01/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import UIKit
import CoreData

class DashBoardVC: BaseVC {
    var products = [productsmodel]()
    var cartProductIds = [String]()
    @IBOutlet weak var productsTV: UITableView!
    @IBOutlet weak var cartCount: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Dashboard"
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getproductdetails() {
        if networkReachability?.isReachable == true {
        APImanager.getproduts(sucess: { (result) in
            self.products = result
            self.fetchcartData()
        }) { (err) in
            print(err)
        }
        
    }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
         self.getproductdetails()
    }
    
    
    @IBAction func logoutAction(_ sender: Any) {
          UserDefaults.standard.set(false, forKey: "LoginStatus")
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialVC: LoginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigation = UINavigationController(rootViewController: initialVC)
        self.present(navigation, animated: false, completion: {
            
        })
    }
    
    @IBAction func goTOCartAction(_ sender: Any) {
        let Checkoutvc = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
        self.navigationController?.pushViewController(Checkoutvc, animated: true)
    }
}
extension DashBoardVC:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let product = products[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTVC", for: indexPath) as? ProductsTVC
        cell?.product_Name.text = product.name
        cell?.product_Image.image = UIImage(named: product.imageurl)
        cell?.product_Price.text = product.price
        cell?.cart_Btn.tag = indexPath.row
        if cartProductIds.count > 0 {
            if cartProductIds .contains(product.Product_id) {
                cell?.cart_Btn.setTitle("Go To Cart", for: .normal)
                }
            else{
                 cell?.cart_Btn.setTitle("Add To Cart", for: .normal)
            }
        }
        else{
            cell?.cart_Btn.setTitle("Add To Cart", for: .normal)
        }

        
        cell?.cart_Btn.addTarget(self, action: #selector(handleaddtocart(sender:)), for: .touchUpInside)
        
        return cell!
    }
    @objc func handleaddtocart(sender:UIButton) {
        
    if cartProductIds.contains(products[sender.tag].Product_id) {
        self.showToast(message: "Already in Cart", font: UIFont.systemFont(ofSize: 14, weight: .black))
            }
        else{
        cartProductIds.append(products[sender.tag].Product_id)
        self.cartproducts.append(products[sender.tag])
        productsTV.reloadData()
        self.showToast(message: "Added To Cart", font: UIFont.systemFont(ofSize: 14, weight: .black))
        self.savecartdetails(product: products[sender.tag])
        }
        cartCount.text = String(cartProductIds.count)
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    func savecartdetails(product : productsmodel){
        print(product)
       
            
            let managedContext = globalclass.shared.managedContext
            
            // 2
            let entity =
                NSEntityDescription.entity(forEntityName: "Products",
                                           in: managedContext)!
            
            let productobject = NSManagedObject(entity: entity,
                                       insertInto: managedContext)
            
            // 3
            productobject.setValue(product.Product_id, forKeyPath: "productID")
            productobject.setValue(product.imageurl, forKeyPath: "productImage")
            productobject.setValue(product.price, forKeyPath: "productPrice")
            productobject.setValue(product.name, forKeyPath: "productName")
            
            
            // 4
            do {
                try managedContext.save()
                
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        
        
    }
    func fetchcartData()  {
        cartProductIds.removeAll()
        let managedContext = globalclass.shared.managedContext
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Products")
       
        
        //3
        do {
            let products = try managedContext.fetch(fetchRequest)
            
            for id in products {
                cartProductIds.append(id.value(forKey: "productID") as! String)
            }
            
            cartCount.text = String(cartProductIds.count)
           
            productsTV.reloadData()
           
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    
}










