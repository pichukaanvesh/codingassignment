//
//  LoginVC.swift
//  CodingAssignment
//
//  Created by Anvesh on 01/07/19.
//  Copyright © 2019 Anvesh. All rights reserved.
//

import UIKit
import CoreData

class LoginVC: BaseVC {
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginsucess() -> Bool{
        let username = UserDefaults.standard.string(forKey: "username")
        let password = UserDefaults.standard.string(forKey: "password")
        if usernameTF.text != "" && passwordTF.text != "" {
            if username == usernameTF.text && password == passwordTF.text
        {
            return true
        }
            else {
                self.showAlert(title: "invalid credentials", message: "Please give Valid credentials", actionTitles: ["ok"], actions: [{ action1 in
                    
                    },nil])
                return false
            }
            
        }            else
            {
                self.showAlert(title: "Please enter all the fields", message: "username and password fields are mandatory", actionTitles: ["ok"], actions: [{ action1 in
                    
                },nil])
                return false
        }
        
       
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        
        if loginsucess()
        {
            UserDefaults.standard.set(true, forKey: "LoginStatus")
        let Dashboardvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        self.navigationController?.pushViewController(Dashboardvc, animated: true)
        }
       
    }
    
    @IBAction func siignupAction(_ sender: Any) {
        let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
       self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
}
